Create a shortcut from `lock.bat`

Set Target to `?:\Path\lock.bat DriveLetter`

For example:

```
C:\_lock.bat E
```

Change icon (optional)

Enable "Run as administrator" (advanced property, required)
